import { createRouter, createWebHistory } from "vue-router";
import HomePageComponent from "@/components/HomePageComponent.vue";
import DayPlan from "@/pages/DayPlan.vue";

const routes = [
  {
    path: "/day/:day",
    name: ":day",
    component: DayPlan,
  },
  {
    path: "/",
    name: "home",
    component: HomePageComponent,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
